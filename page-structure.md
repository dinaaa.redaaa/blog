```
blog
│   README.md
│
└─── src
|   |   index.js
|   |   index.css
|   |
|   └─── app
│   |   |   App.css
|   |   |   App.js
|   |   |
|   |   └─── home
|   │   |   │   home.jsx
|   |   |   |   home.scss  
|   │   |
|   |   └─── posts
|   │   |   |   Posts.jsx
|   |   |   |   posts.scss
|   |   │   |   posts.service.js
|   │   │   |   post.component.jsx
|   |   |
|   |   └─── categories
|   |   │   |   Categories.jsx
|   |   │   |   category.service.js
|   |   │   |   categories.scss
|   |   |
|   |   └─── user
|   |   │   |   Login.jsx
|   |   │   |   Register.jsx
|   |   │   |   auth.service.js
|   |   │   |   auth.scss
|   │   |
|   |   └─── shared
|   |   |	└─── header
|   │   |   │   |   AppHeader.jsx
|   │   |   │   |   app-header.scss
|   │     
|   └─── assets
|   |	└─── images
|   | 	│   │   logo.png
|   |	│   
```
