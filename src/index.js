import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './app/App';
import * as serviceWorker from './serviceWorker';
import axios from 'axios';

const token = localStorage.getItem('token');
axios.defaults.headers.common = {'Authorization': `Token ${token}`}

axios.interceptors.request.use(request => {
  return request
}, error => {
  return Promise.reject(error);
})

axios.interceptors.response.use(response => {
  return response
}, error => {
  return Promise.reject(error);
})

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
