import React, { Component } from 'react';
import  { Posts } from '../Posts/Posts';

import './home.scss';

export default class Home extends Component{
  render () {
    return (
      <div className="home">
        <h1 className="home__title">posts</h1>
        <Posts />
      </div>
    );
  }
}
