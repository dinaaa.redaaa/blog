import React, { Component } from 'react';
import { Link } from "react-router-dom";
import './categories.scss';
import { getCategories } from './categories.service'

export class Categories extends Component{
  state = {
    categories : []
  };

  componentDidMount () {
    getCategories().then(response => {
      this.setState({
        categories : response
      });
    });
  }

  render () {
    const renderCategories = this.state.categories.map(category => {
      return (
        <div className="categories" key={category.id}>
          <p onClick={() => this.props.getCategoryPosts(category.id)} className="categories__title">{category.body}</p>
        </div>
      )
    });

    return (
      <div className="categories__container">{renderCategories}</div>
    );
  }
}
