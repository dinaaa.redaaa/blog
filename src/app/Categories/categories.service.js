import axios from 'axios'

export function getCategories () {
  return axios.get("https://my-json-server.typicode.com/dinaredaa/blog-data/categories")
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log(error);
    });
}

export function getCategoryPosts (id) {
  return axios.get(`https://my-json-server.typicode.com/dinaredaa/blog-data/posts?categoryId=${id}`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log(error);
    });
}
