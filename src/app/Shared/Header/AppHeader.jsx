import React, { Component } from 'react';
import { NavLink } from 'react-router-dom'

import './app-header.scss';

class AppHeader extends Component {
  render() {
    return (
      <div className="navbar">
        <div className="container">
          <a href="/" className="logo">Blog</a>
          <ul>
            <NavLink exact activeClassName="selected" to="/">Home</NavLink>
            <NavLink activeClassName="selected" to="/about">About</NavLink>
            <NavLink exact activeClassName="selected" to="/posts">Posts</NavLink>
            { localStorage.getItem('token') == null ? <NavLink exact activeClassName="selected" to="/login">Login</NavLink> : '' }
          </ul>
        </div>
      </div>
    )
  }
}

export default AppHeader;
