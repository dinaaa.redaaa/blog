import React, { Component } from 'react';
import './auth.scss';
import {LoginUser} from './auth.service'

export class Login extends Component{
  state = {
    // setting data here just for fake api
    "email": "eve.holt@reqres.in",
    "password": "pistol"
  };

  handleChange= (event) => {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  };

  handleSubmit= (event) => {
    event.preventDefault();
    LoginUser(this.state).then(() => {
      this.props.history.push("/");
    });
  };

  render () {
    return (
      <div className="">
        <h1 className="home__title">Login</h1>
        <form onSubmit={this.handleSubmit}>
          <label>
            Email:
            <input name="email" type="email" value={this.state.email} onChange={this.handleChange} />
          </label>
          <label>
            Password:
            <input name="password" type="password" value={this.state.password} onChange={this.handleChange} />
          </label>
          <input type="submit" className="btn-submit"  value="Submit" />
        </form>
      </div>
    );
  }
}
