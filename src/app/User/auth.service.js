import axios from 'axios'

export function RegisterUser (data) {
  return axios.post("https://reqres.in/api/register", data)
    .then(response => {
      localStorage.setItem('token', response.data['token']);
      localStorage.setItem('id', response.data['id']);
      return response.data;
    })
    .catch(error => {
      console.log(error);
    });
}

export function LoginUser (data) {
  return axios.post("https://reqres.in/api/login", data)
    .then(response => {
      localStorage.setItem('token', response.data['token']);
      return response.data;
    })
    .catch(error => {
      console.log(error);
    });
}
