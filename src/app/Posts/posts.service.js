import axios from 'axios'

export function getPosts () {
   return axios.get("https://my-json-server.typicode.com/dinaredaa/blog-data/posts")
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log(error);
    });
}

export function getPostDetails(id) {
  return axios.get(`https://my-json-server.typicode.com/dinaredaa/blog-data/posts/${id}`)
    .then(response => {
      return response.data;
    })
    .catch(error => {
      console.log(error);
    });
}
