import React, { Component } from 'react';

import {getCategoryPosts} from '../Categories/categories.service'

import  { Posts } from './Posts';
import { Categories } from '../Categories/Categories'

import './posts.scss';

export default class PostsPage extends Component{
  state = {
    posts: []
  };

  getCategoryPosts = (id) => {
    getCategoryPosts(id).then(response => {
      this.setState({
        posts : response
      });
    });
  }

  render () {
    return (
      <div>
        <h1 className="blog__title">Posts</h1>
        <div className="blog__container">
          <Categories getCategoryPosts={this.getCategoryPosts} />
          <Posts posts={this.state.posts} />
        </div>
      </div>
    );
  }
}
