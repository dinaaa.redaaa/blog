import React, { Component } from 'react';
import {getPostDetails} from "./posts.service";

class Post extends Component {
  state = {
    post : {}
  };

  componentDidMount () {
    getPostDetails(`${this.props.match.params.id}`).then(response => {
      this.setState({
        post : response
      });
    });
  }

  render () {
    return (
      <div className="container blog-container page__no--background">
        <div className="post">
          <h3>{this.state.post.title}</h3>
          <p>{this.state.post.body}</p>
        </div>
      </div>
    );
  }
}

export default Post;
