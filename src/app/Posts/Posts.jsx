import React, { Component } from 'react';
import {Link} from "react-router-dom";
import './posts.scss';
import { getPosts } from './posts.service'

export class Posts extends Component{
  state = {
    currentPage: 1,
    postsPerPage: 6,
    posts: []
  };

  componentDidMount () {
    getPosts().then(response => {
      this.setState({
        posts : response
      });
    });
  }

  handleClick = (event) => {
    this.setState({
      currentPage: Number(event.target.id)
    });
  };

  render() {
    const posts = (!this.props.posts || this.props.posts.length === 0) ? this.state.posts : this.props.posts;
    const { currentPage, postsPerPage } = this.state;

    const indexOfLastTodo = currentPage * postsPerPage;
    const indexOfFirstTodo = indexOfLastTodo - postsPerPage;
    const currentPosts = posts.slice(indexOfFirstTodo, indexOfLastTodo);

    const pageNumbers = [];
    for (let i = 1; i <= Math.ceil(posts.length / postsPerPage); i++) {
      pageNumbers.push(i);
    }

    const renderPageNumbers = pageNumbers.map(number => {
      return (
        <li
          key={number}
          id={number}
          onClick={this.handleClick}
          className= {currentPage===number? "active" : ""}>
          {number}
        </li>
      );
    });

    const renderPosts = currentPosts.map(post => {
      return (
        <div className="blog" key={post.id}>
          <div className="blog__content">
            <Link to={`/posts/${post.id}`} className="blog__content__title">{post.title}</Link>
            <p className="blog__content__body">{post.body}</p>
          </div>
        </div>
      )
    });

    return (
      <div className="posts__container">
        <div>{renderPosts}</div>
        <ul id="page-numbers">
          {renderPageNumbers}
        </ul>
      </div>
    );
  }
}
