import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

import './App.css';
import Home from './Home/Home'
import AppHeader from "./Shared/Header/AppHeader";
import Post from "./Posts/Post";
import PostsPage from './Posts/postsPage'
import {Login} from "./User/Login";
import {Register} from './User/Register'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <AppHeader/>
        <Switch>
          <Route exact path="/" component={Home}/>
          <Route exact path="/posts" component={PostsPage}/>
          <Route path="/posts/:id" component={Post}/>
          <Route path="/login" component={Login}/>
          <Route path="/register" component={Register}/>
        </Switch>
      </div>
    </BrowserRouter>
  );
}

export default App;
