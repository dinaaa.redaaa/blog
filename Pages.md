## Pages

# Home page:
- Latest posts (image, title, little-description, date)

# Category page:
- Contains category posts
- Sidemenu have sub categories if it has 

# Post page:
- Post details (images, title, description, date)
- Comments on post
- Related posts

# Login page:
- Email, password and remember me 

# Register page:
- Email, password not less than 8, confirm password, name and his image

# Each page will have a header which contains (Home, About, Search, Login, Register, profile name and profile image(small))
